# SteeleyeBackend
API Developer Assessment for Steeleye. This is a REST API for managing trades. It provides endpoints for listing, creating, getting, updating, and deleting trades.

Check it out here: https://steeleye-flk6.onrender.com/docs

## Authentication
There is no authentication required to access the endpoints.

## Endpoints

### `GET /trades`
This endpoint retrieves a list of trades filtered by the given parameters. 

#### Query Parameters

- `search` (optional): A string to search for in counterparty, instrumentId, instrumentName, and trader fields.
- `assetClass` (optional): The asset class to filter by.
- `start` (optional): The start date and time of the trade in ISO format.
- `end` (optional): The end date and time of the trade in ISO format.
- `minPrice` (optional): The minimum trade price to filter by.
- `maxPrice` (optional): The maximum trade price to filter by.
- `tradeType` (optional): The trade type to filter by ("BUY" or "SELL").
- `limit` (optional): The maximum number of trades to return (default: 10).
- `skip` (optional): The number of trades to skip (default: 0).
- `sort` (optional): The field to sort by and the sort direction separated by a comma (default: "tradeDateTime,desc").

### `POST /trades`
This endpoint creates a new trade.

#### Request Body
- `trade_id`: A string representing the trade ID.
- `counterparty`: A string representing the counterparty.
- `instrument_id`: A string representing the instrument ID.
- `instrument_name`: A string representing the instrument name.
- `trader`: A string representing the trader.
- `asset_class`: A string representing the asset class.
- `trade_details`: An object containing trade details including `price` (float) and `buy_sell_indicator` ("BUY" or "SELL").

### `GET /trades/{trade_id}`
This endpoint retrieves a specific trade by ID.

### `PUT /trades/{trade_id}`
This endpoint updates an existing trade.

#### Request Body
- `trade_id`: A string representing the trade ID.
- `counterparty`: A string representing the counterparty.
- `instrument_id`: A string representing the instrument ID.
- `instrument_name`: A string representing the instrument name.
- `trader`: A string representing the trader.
- `asset_class`: A string representing the asset class.
- `trade_details`: An object containing trade details including `price` (float) and `buy_sell_indicator` ("BUY" or "SELL").

### `DELETE /trades/{trade_id}`
This endpoint deletes a specific trade by ID.
