import datetime
import os
from pathlib import Path

import pymongo
from dotenv import load_dotenv
from fastapi import APIRouter, Depends, HTTPException, Query
from pymongo import MongoClient

from schema import Trade

router = APIRouter(
    prefix="/trades",
    tags=["Trades"]
)

if os.path.exists('.env'):
    dotenv_path = Path('.env')
    load_dotenv(dotenv_path=dotenv_path)


def get_db():
    client = MongoClient(os.getenv("MONGODB_URL"))
    db = client.steeleye
    yield db
    client.close()


@router.get("", response_model=list[Trade])
async def list_trades(
        db=Depends(get_db),
        search: str = Query(None),
        assetClass: str = Query(None),
        start: datetime.datetime = Query(None),
        end: datetime.datetime = Query(None),
        minPrice: float = Query(None),
        maxPrice: float = Query(None),
        tradeType: str = Query(None),
        limit: int = Query(10),
        skip: int = Query(0),
        sort: str = Query("tradeDateTime,desc")
):
    query = {}

    if search:
        query = {"$or": [
            {"counterparty": {"$regex": search, "$options": "i"}},
            {"instrumentId": {"$regex": search, "$options": "i"}},
            {"instrumentName": {"$regex": search, "$options": "i"}},
            {"trader": {"$regex": search, "$options": "i"}}
        ]}

    if assetClass:
        query["assetClass"] = assetClass

    if start and end:
        query["tradeDateTime"] = {"$gte": start, "$lte": end}
    elif start:
        query["tradeDateTime"] = {"$gte": start}
    elif end:
        query["tradeDateTime"] = {"$lte": end}

    if minPrice and maxPrice:
        query["tradeDetails.price"] = {"$gte": minPrice, "$lte": maxPrice}
    elif minPrice:
        query["tradeDetails.price"] = {"$gte": minPrice}
    elif maxPrice:
        query["tradeDetails.price"] = {"$lte": maxPrice}

    if tradeType:
        query["tradeDetails.buySellIndicator"] = tradeType

    trades = []
    sort_field, sort_direction = sort.split(",")
    sort_direction = pymongo.DESCENDING if sort_direction.lower() == "desc" else pymongo.ASCENDING
    for trade in db.trades.find(query).sort(sort_field, sort_direction).skip(skip).limit(limit):
        trades.append(Trade(**trade))
    return trades


@router.post("", response_model=Trade)
async def create_trade(trade: Trade, db=Depends(get_db)):
    trade = db.trades.find_one({"tradeId": trade.trade_id})
    if trade:
        raise HTTPException(status_code=400, detail="Trade already exists")

    result = db.trades.insert_one(trade.dict(by_alias=True))
    if not result.inserted_id:
        raise HTTPException(status_code=500, detail="Trade not created")

    trade_dict = trade.dict(by_alias=True)
    return Trade(**trade_dict)


@router.get("/{trade_id}", response_model=Trade)
async def get_trade_by_id(trade_id: str, db=Depends(get_db)):
    trade = db.trades.find_one({"tradeId": trade_id})
    if not trade:
        raise HTTPException(status_code=404, detail="Trade not found")
    return Trade(**trade)


@router.put("/{trade_id}", response_model=Trade)
async def update_trade(trade_id: str, trade: Trade, db=Depends(get_db)):
    result = db.trades.replace_one(
        {"tradeId": trade_id},
        trade.dict(by_alias=True)
    )
    if result.matched_count == 0:
        raise HTTPException(status_code=404, detail="Trade not found")
    trade_dict = trade.dict(by_alias=True)
    return Trade(**trade_dict)


@router.delete("/{trade_id}")
async def delete_trade(trade_id: str, db=Depends(get_db)):
    result = db.trades.delete_one({"tradeId": trade_id})
    if result.deleted_count == 0:
        raise HTTPException(status_code=404, detail="Trade not found")
    return {"message": "Trade deleted successfully"}
