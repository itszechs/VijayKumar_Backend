from fastapi import FastAPI

import trades

app = FastAPI(
    title="SteelEye Backend",
    contact={
        "name": "zechs",
        "url": "https://github.com/itszechs",
    }
)

app.include_router(trades.router)
